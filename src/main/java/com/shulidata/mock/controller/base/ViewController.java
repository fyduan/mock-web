package com.shulidata.mock.controller.base;

import com.google.common.base.Joiner;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Fyduan on 2016/4/30.
 */
@Controller
public class ViewController {

    @RequestMapping(method = RequestMethod.GET, value = "/{path1}/{path2}/{path3}.htm")
    public String dis3(@PathVariable("path1") String path1,
                       @PathVariable("path2") String path2,
                       @PathVariable("path3") String path3) {
        return build(path1, path2, path3);
    }
    @RequestMapping(method = RequestMethod.GET, value = "/{path1}/{path2}.htm")
    public String dis2(@PathVariable("path1") String path1,
                       @PathVariable("path2") String path2) {
        return build(path1, path2);
    }
    @RequestMapping(method = RequestMethod.GET, value = "/{path1}.htm")
    public String dis1(@PathVariable("path1") String path1) {
        return build(path1);
    }

    private String build(String...paths) {
        return Joiner.on("/").join(paths);
    }

}
