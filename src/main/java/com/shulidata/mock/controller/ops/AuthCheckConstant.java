package com.shulidata.mock.controller.ops;

/**
 * Created by jiangdawei on 16/7/4.
 */
public class AuthCheckConstant {
    /** RSA最大加密长度，block_size = rsakey_size/8 - 11  */
    public static final int    MAX_ENCRYPT_BLOCK = 53;

    /** RSA最大解密长度，block_size = rsakey_size/8  */
    public static final int    MAX_DECRYPT_BLOCK = 64;

    /** 加密解密算法 */
    public static final String KEY_ALGORITHM     = "RSA";

    /** 编码字符集 */
    public static final String CODE_SET          = "UTF-8";

    /** 签名key值 */
    public static final String SIGN_NAME         = "signName";

    /** 混合盐key值 */
    public static final String MIX_SALT          = "mixSalt";

    /** 业务机构key值 */
    public static final String ORG_CODE          = "orgCode";

    /** 业务数据源key值 */
    public static final String DATA_SOURCE       = "dataSource";

    /** 业务类型key值 */
    public static final String BIZ_TYPE          = "bizType";

    /** 业务号key值 */
    public static final String BIZ_NO            = "bizNo";

    /** 业务附加字段key值 */
    public static final String EXTRA_DATA        = "extraData";

    /** 数据采集平台机构编号 */
    public static final String DTAUTH_ORG_CODE   = "DTAUTH";
}
