package com.shulidata.mock.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author fyduan
 * @date 16/4/29
 */
@Getter
@Setter
public class MockConf implements Serializable {

    private String id; // UUID,MOCK编号
    private String api; // 接口名称
    private String server; // 接口所属服务
    private String script; // mock脚本
    private String scriptMethod; // 脚本暴露的方法
    private String desc; // 接口描述
    private long modifyTime; // 最后变更时间
    private boolean deleted;

}
