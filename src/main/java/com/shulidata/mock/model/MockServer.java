package com.shulidata.mock.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author fyduan
 * @date 16/4/29
 */
@Getter
@Setter
public class MockServer implements Serializable {
    private String id;
    private String name;
}
