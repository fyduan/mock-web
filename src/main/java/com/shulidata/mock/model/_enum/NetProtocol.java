package com.shulidata.mock.model._enum;

import lombok.Getter;

/**
 * 网络协议.
 *
 * @author fyduan
 * @date 16/7/29
 */
@Getter
public enum NetProtocol {
    HTTP("http://"), HTTPS("https://");

    private final String value;

    NetProtocol(String value) {
        this.value = value;
    }
}
