package com.shulidata.mock.repositories;

import com.shulidata.mock.model.MockConf;

import java.util.List;

/**
 * @author fyduan
 * @date 16/4/29
 */
@MyBatisRepository
public interface MockConfRepository {

    void save(MockConf conf);

    int update(MockConf conf);

    MockConf get(String id);

    MockConf findByServerAndApi(String server, String api);

    List<MockConf> loadAll();

    List<MockConf> findAfterByTime(long time);

    int delete(String id);
}
