package com.shulidata.mock.repositories;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 该注解用于标识MyBatis的DAO，便于{@link org.mybatis.spring.mapper.MapperScannerConfigurer}的扫描
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Component
public @interface MyBatisRepository {
    String value() default "";
}
