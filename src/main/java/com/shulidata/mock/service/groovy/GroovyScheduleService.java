package com.shulidata.mock.service.groovy;

import com.shulidata.mock.service.MockConfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author fyduan
 * @date 16/4/29
 */
@Component
public class GroovyScheduleService {

    @Autowired
    GroovyManager groovyManager;

    @Scheduled(fixedDelay = 60000)
    public void toLoadScript() {
        groovyManager.refresh();
    }

}
