package com.shulidata.mock.service.groovy;

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;
import groovy.lang.Script;
import lombok.Getter;

/**
 * @author fyduan
 * @date 16/4/29
 */
@Getter
public class GroovyScriptProcessor {

    private boolean initSuccess = false;
    private GroovyObject groovyObject;
    private String invokeMethod; // 脚本暴露的方法,供给处理器调用

    public GroovyScriptProcessor(String method, String script) {
        groovyObject = GroovyUtils.newInstance(script);
        // 如果创建Groovy对象结果为NULL,则认为初始化失败
        if (groovyObject != null)
            initSuccess = true;
        invokeMethod = method;
    }

    // 执行脚本方法
    public Object invoke(Object params) {
        return groovyObject.invokeMethod(invokeMethod, params);
    }
}
