package com.shulidata.mock.service.groovy;

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;
import groovy.lang.Script;
import org.springframework.util.Assert;

/**
 * @author fyduan
 * @date 16/4/29
 */
public class GroovyUtils {

    public static GroovyObject newInstance(String script) {
        GroovyObject groovyObject = null;
        try {
            Class clz = new GroovyClassLoader().parseClass(script);
            groovyObject = (GroovyObject) clz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            // TODO LOG...
        }
        return groovyObject;
    }

    public static Class parseScript(String script) {
        Class clazz = null;
        try {
            clazz = new GroovyClassLoader().parseClass(script);
        } catch (Exception e) {
            // LOG...
        }
        return clazz;
    }

}

