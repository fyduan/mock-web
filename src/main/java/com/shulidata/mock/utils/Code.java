package com.shulidata.mock.utils;

/**
 * 业务状态码枚举类接口
 * @author FY
 */
public interface Code {
    /**
     * 返回业务状态代码
     */
    int getValue();

    /**
     * 返回业务状态描述
     */
    String getInfo();
}
