/**
 * Shuli.com Inc.
 * Copyright (c) 2015-2016 All Rights Reserved.
 */
package com.shulidata.mock.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.text.ParseException;
import java.util.Date;

/**
 * 日期格式化工具
 *
 * @author ning.k
 * @version $Id: DateExtUtils.java, v 0.1 2016年5月17日 下午4:55:28 ning.k Exp $
 */
public class DateExtUtils extends DateUtils {

    public static class Formatter extends DateFormatUtils {
        public static final String FORMAT_YYYYMMDD = "yyyyMMdd";
        public static final String FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
        public static final String FORMAT_YYYY_MM_DD_CN = "yyyy年MM月dd日";
        public static final String FORMAT_DATETIME_SPACE = "yyyy-MM-dd HH:mm:ss";
        public static final String FORMAT_DATETIME = "yyyyMMddHHmmss";
        public static final String FORMAT_DATETIME_MILLIS = "yyyyMMddHHmmssSSS";

        /**
         * 线程安全的日期快速格式化工具 - yyyyMMdd
         */
        public static final FastDateFormat FORMATTER_YYYYMMDD = FastDateFormat.getInstance(FORMAT_YYYYMMDD);
        /**
         * 线程安全的日期快速格式化工具 - yyyy-MM-dd
         */
        public static final FastDateFormat FORMATTER_YYYY_MM_DD = ISO_DATE_FORMAT;
        /**
         * 线程安全的日期快速格式化工具 - yyyy年MM月dd日
         */
        public static final FastDateFormat FORMATTER_YYYY_MM_DD_CN = FastDateFormat.getInstance(FORMAT_YYYY_MM_DD_CN);
        /**
         * 线程安全的日期快速格式化工具 - yyyy-MM-dd HH:mm:ss
         */
        public static final FastDateFormat FORMATTER_DATETIME_SPACE = FastDateFormat.getInstance(FORMAT_DATETIME_SPACE);
        /**
         * 线程安全的日期快速格式化工具 - yyyyMMddHHmmss
         */
        public static final FastDateFormat FORMATTER_DATETIME = FastDateFormat.getInstance(FORMAT_DATETIME);
        /**
         * 线程安全的日期快速格式化工具 - yyyyMMddHHmmssSSS
         */
        public static final FastDateFormat FORMATTER_DATETIME_MILLIS = FastDateFormat.getInstance(FORMAT_DATETIME_MILLIS);
    }

    /**
     * 获取时间格式化字符串
     *
     * @param date   {@link Date} 日期对象
     * @param format 格式
     * @return 返回处理后的时间字符串
     */
    public static String getDateString(Date date, String format) {
        if (date == null) {
            return "";
        }
        if (StringUtils.isBlank(format)) {
            format = Formatter.FORMAT_DATETIME_SPACE;
        }
        return Formatter.format(date, format);
    }

    /**
     * 转换时间字符转格式
     *
     * @param dateStr   时间字符串
     * @param orgFormat 原时间格式
     * @param dstFormat 目标时间格式
     * @return 目标格式时间字符串
     */
    public static String changeFormat(String dateStr, String orgFormat, String dstFormat) {
        if (StringUtils.isBlank(dateStr)) {
            return "";
        }
        if (StringUtils.isBlank(orgFormat) || StringUtils.isBlank(dstFormat)) {
            return dateStr;
        }
        final FastDateFormat orgFDF = FastDateFormat.getInstance(orgFormat);
        final FastDateFormat dstFDF = FastDateFormat.getInstance(dstFormat);

        Date temp = null;
        try {
            temp = orgFDF.parse(dateStr);
        } catch (ParseException e) {
            return dateStr;
        }
        return dstFDF.format(temp);
    }

}
