package com.shulidata.mock.utils;

/**
 * 全局通用异常类,只添加了一个 code 属性，见{@link Code}
 */
public class GenericException extends Exception {

    private Code code;

    public GenericException() {
        super();
    }

    public GenericException(Code code) {
        super(code.getInfo());
        this.code = code;
    }
    public GenericException(Code code, String message) {
        super(message);
        this.code = code;
    }
    public GenericException(Code code, Throwable cause) {
        super(code.getInfo(), cause);
        this.code = code;
    }
    public GenericException(Code code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }

}
