package com.shulidata.mock.utils;

import lombok.Getter;

import java.util.Map;

/**
 * Created by Fyduan on 2016/4/30.
 */
@Getter
public enum ParamType {
    LONG(1, Long.class),
    INTEGER(2, Integer.class),
    STRING(3, String.class),
    BOOLEAN(4, Boolean.class),
    MAP(5, Map.class)
    ;

    private final int code;
    private final Class clazz;

    ParamType(int code, Class clazz) {
        this.code = code;
        this.clazz = clazz;
    }
}
