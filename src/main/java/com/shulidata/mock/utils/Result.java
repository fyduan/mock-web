package com.shulidata.mock.utils;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author fyduan
 * @date 16/4/29
 */
@Getter
@Setter
public class Result<T> implements Serializable {

    private int code;
    private String msg;
    private T data;

    private Result(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public Result(Code code, String msg, T data) {
        this(code.getValue(), msg, data);
    }

    public Result(Code code, String msg) {
        this(code.getValue(), msg, null);
    }

    public Result(Code code, T data) {
        this(code.getValue(), null, data);
    }

    public Result(Code code) {
        this.code = code.getValue();
    }

    public Result setUp(Code code, String msg, T data) {
        this.code = code.getValue();
        this.msg = msg;
        this.data = data;
        return this;
    }

    public Result setUp(Code code, String msg) {
        return setUp(code, msg, null);
    }

    public Result setUp(Code code, T data) {
        return setUp(code, null, data);
    }

    public Result setUp(Code code) {
        return setUp(code, null, null);
    }

}
