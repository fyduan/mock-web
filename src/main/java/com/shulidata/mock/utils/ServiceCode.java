package com.shulidata.mock.utils;

import lombok.Getter;

/**
 * @author fyduan
 * @date 16/4/29
 */
@Getter
public enum ServiceCode implements Code {
    OK(0, "操作成功"), ERR_SERVER(-1, "系统错误"),
    ERR_PARAM(50001, "参数错误"),
    ERR_RECORD_EXITS(50011, "记录已经存在"),
    ;

    private final int value;
    private final String info;
    ServiceCode(int value, String info) {
        this.value = value;
        this.info = info;
    }
}
