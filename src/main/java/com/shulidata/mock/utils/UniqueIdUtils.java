package com.shulidata.mock.utils;

import java.util.UUID;

/**
 * Created by Fyduan on 2016/4/30.
 */
public class UniqueIdUtils {

    public static String uuid() {
        return UUID.randomUUID().toString();
    }

    public static String compressUuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }

}
