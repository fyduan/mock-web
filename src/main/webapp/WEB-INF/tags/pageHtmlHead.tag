<%@ tag pageEncoding="UTF-8" %>
<%
    String projectPath = request.getContextPath() + "/";
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+projectPath;
%>
<%@ attribute name="title" type="java.lang.String" required="false" %>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="window-target" content="_top">

<title>${title} - MOCK服务</title>

<link rel="stylesheet" href="resources/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="resources/scripts/jquery/jquery-1.12.3.js"></script>
<script src="resources/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="resources/scripts/common.js" ></script>
<script type="text/javascript">
    var __basePath__ = '<%=projectPath%>';
    var currentURL = "${requestScope.currentURL}";
</script>
