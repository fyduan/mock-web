<%--
  Created by IntelliJ IDEA.
  User: Fyduan
  Date: 2016/4/30
  Time: 20:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/common/taglibs.jspf" %>
<html>
<head>
    <mw:pageHtmlHead title="编辑MOCK配置" />
</head>
<body>
<mw:pageBodyHeader/>
<main class="container">

    <div id="paramsForm" class="">
        <div class="form-group hide">
            <label for="mockId" class="control-label">MOCK编号:</label>
            <div class="">
                <input type="text" class="form-control" id="mockId" name="id" data-notnull="false" placeholder="" disabled>
            </div>
        </div>
        <div class="form-group">
            <label for="server" class="control-label">服务名称:</label>
            <div class="">
                <input type="text" class="form-control" id="server" name="server" data-notnull="true" data-alert="服务名称不能为空" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="api" class="control-label">服务接口:</label>
            <div class="">
                <input type="text" class="form-control" id="api" name="api" data-notnull="true" data-alert="服务接口不能为空" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="desc" class="control-label">服务描述:</label>
            <div class="">
                <input type="text" class="form-control" id="desc" name="desc" data-notnull="false" data-alert="服务描述不能为空" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="scriptMethod" class="control-label">暴露方法:</label>
            <div class="">
                <input type="text" class="form-control" id="scriptMethod" name="scriptMethod" data-notnull="true" data-alert="脚本中暴露的方法名不能为空" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="script" class="control-label">脚本内容:</label>
            <div class="">
                <textarea rows="6" id="script" name="script" class="form-control" data-notnull="true" data-alert="MOCK脚本内容不能为空"></textarea>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" id="submit" class="btn btn-primary">提交</button>
            <button type="submit" id="delete" class="btn btn-danger">删除</button>
        </div>
    </div>

</main>
<footer></footer>

<script>
    $(function(){
        // 编辑
        var $mockId = $('#mockId');
        window._edit_id_ = CMM.getSearchParam('id', location.search);
        if (!CMM.isBlank(_edit_id_)) {
            $mockId.parent().parent().removeClass('hide');
            $mockId.val(_edit_id_);

            // 载入数据
            loadData();
        }

        // 提交
        $('#submit').on("click", function(){
            $(this).attr('disabled', true);
            var params = validAndPackingParams($('#paramsForm').find('input[type=text],textarea'));
            if (params == null) return;
            // Ajax提交
            $.post('mock/conf', params, function(data){
                var msg = data.msg;
                if (data.code == 0) {
                    alert("编辑成功");
                    window.location.href = 'list.htm';
                    return;
                }
                alert(msg);
                $('#server').select();
                $('#submit').attr('disabled', false);
            }).error(function (data) {
                alert('网络错误，请重试');
                $('#submit').attr('disabled', false);
            });
        });

        // 删除
        $('#delete').on('click', function (e) {
            $(this).attr('disabled', true);
            $.ajax({
                url: 'mock/conf?id='+_edit_id_,
                type: 'DELETE',
                success: function (data) {
                    if (data.code == 0) {
                        alert("删除成功，返回列表");
                        window.location.href = 'list.htm';
                        return;
                    }

                    alert(msg);
                    $('#delete').attr('disabled', false);
                },
                error: function (data) {
                    alert('网络错误，请重试');
                    $('#delete').attr('disabled', false);
                }
            })
        })
    });

    function loadData() {
        $.get('mock/conf/info?id='+_edit_id_, function (data) {
            if (data.code != 0) {
                alert(data.msg);
                window.location.href = 'list.htm';
                return false;
            }
            writeDateToDom(data.data);
        }).error(function (data) {
            alert('网络错误，返回列表页');
            window.location.href = 'list';
            return false;
        });
    }

    function validAndPackingParams($inps) {
        var params = {};
        for(var i = 0; i < $inps.size(); i++) {
            var $e = $inps.eq(i), notnull = $e.data('notnull'), val = $e.val();
            if (notnull && CMM.isBlank(val)) {
                alert($e.data("alert"));
                $e.focus();
                return null;
            }
            params[$e.attr('name')] = val;
        }
        return params;
    }

    function writeDateToDom(info) {
        $.each(info, function (k, v) {
            $('#'+k).val(v);
        })
    }
</script>
</body>
</html>