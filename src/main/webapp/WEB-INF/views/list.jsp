<%--
  Created by IntelliJ IDEA.
  User: Fyduan
  Date: 2016/4/30
  Time: 20:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/common/taglibs.jspf" %>
<html>
<head>
    <mw:pageHtmlHead title="列表查看" />
    <style>
        #confTable > tbody > tr pre {
            max-height: 500px;
        }
    </style>
</head>
<body>
    <mw:pageBodyHeader/>

    <main class="container">
        <table id="confTable" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th style="width: 280px;">编号</th>
                    <th>服务</th>
                    <th>接口</th>
                    <th>脚本内容</th>
                    <th>脚本暴露方法</th>
                    <th>描述</th>
                    <th style="width: 140px;">操作</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </main>
    <footer class="footer hidden-print">

    </footer>

    <%-- Script Content --%>
    <script id="templateTr" type="text/html">
        <tr data-conf-id="{id}">
            <th scope="row">{id}</th>
            <td>{server}</td>
            <td>{api}</td>
            <td><pre>{script}</pre></td>
            <td><kbd>{scriptMethod}</kbd></td>
            <td>{desc}</td>
            <td>
                <button type="button" data-ev="preview" class="btn btn-link btn-xs">预览</button>
                <button type="button" data-ev="edit" class="btn btn-link btn-xs">编辑</button>
                <button type="button" data-ev="delete" class="btn btn-link btn-xs">删除</button>
            </td>
        </tr>
    </script>
    <script>
        var $table = $('#confTable'), $templateTr = $('#templateTr');
        $(function () {
            // 加载数据
            $.get("/mock-web/mock/confs", function (data) {
                if (data.code == 0) {
                    writeDataToTable(data.data);
                } else
                    writeEmptyAlert(data.msg);
            });

            // 操作事件
            $table.on('click', 'button', function (e) {
                var $btn = $(this), ev = $btn.data('ev'), $tr = $btn.closest('tr');
                if (ev == 'preview') {
                    window.location.href = 'preview.htm?id=' + $tr.data('conf-id');
                }
                // 编辑
                else if (ev == 'edit') {
                    window.location.href = 'edit.htm?id=' + $tr.data('conf-id');
                }
                else if (ev == 'delete') {
                    // TODO ajax delete a record, and remove $tr.
                    $.ajax({
                        url: '/mock-web/mock/conf',
                        type: 'DELETE',
                        dataType: 'json',
                        success: function (data) {
                            if (data.code == 0) {
                                alert('删除成功.');
                                $tr.remove();
                            } else {
                                alert(data.msg);
                            }
                        }
                    })
                }
            })
        });

        function writeDataToTable(list) {
            var $tbody = $table.find('tbody');
            for (var i = 0, len = list.length; i < len; i++) {
                var o = list[i], trHtml = $templateTr.html();
                $.each(list[i], function (k, v) {
                    trHtml = trHtml.replace(new RegExp('{' + k + '}', 'g'), v);
                });

                $tbody.append(trHtml)
            }
        }

        function writeEmptyAlert(msg) {
            $tbody('<tr>' + msg + '</tr>');
        }
    </script>
</body>
</html>
