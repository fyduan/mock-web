<%--
  Created by IntelliJ IDEA.
  User: Fyduan
  Date: 2016/4/30
  Time: 20:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/common/taglibs.jspf" %>
<html>
<head>
    <mw:pageHtmlHead title="编辑MOCK配置" />
</head>
<body>
<mw:pageBodyHeader/>
<main class="container">

    <div id="paramsForm" class="">
        <div class="form-group">
            <label for="hostname" class="control-label">访问域名:</label>
            <div class="">
                <input type="text" class="form-control" id="hostname" name="hostname" value="localhost" data-notnull="false" placeholder="访问域名不能为空">
            </div>
        </div>
        <div class="form-group">
            <label for="port" class="control-label">端口号:</label>
            <div class="">
                <input type="text" class="form-control" id="port" name="port" value="8080" data-notnull="false" placeholder="port不能为空">
            </div>
        </div>
        <div class="form-group">
            <label for="path" class="control-label">接口路径:</label>
            <div class="">
                <input type="text" class="form-control" id="path" name="path" value="/dataSource/queryList.htm" data-notnull="true" data-alert="接口路径不能为空" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="orgCode" class="control-label">商户代码:</label>
            <div class="">
                <input type="text" class="form-control" id="orgCode" name="orgCode" value="DATAHUB" data-notnull="true" data-alert="商户代码不能为空" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="dataSource" class="control-label">数据源:</label>
            <div class="">
                <input type="text" class="form-control" id="dataSource" name="dataSource" value="ACCUFUND" data-notnull="true" data-alert="数据源不能为空" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="extraData" class="control-label">扩展参数:</label>
            <div class="">
                <textarea rows="6" class="form-control" id="extraData" name="extraData" data-notnull="false" data-alert="" placeholder=""></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="privateKey" class="control-label">商户私钥:</label>
            <div class="">
                <textarea rows="6" id="privateKey" name="privateKey" class="form-control" data-notnull="false" data-alert=""></textarea>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" id="submit" class="btn btn-primary">提交</button>
            <button type="submit" id="delete" class="btn btn-danger">删除</button>
        </div>
    </div>

    <div>

        <div class="form-group">
            <label for="url" class="control-label">加密后的地址:</label>
            <div class="">
                <textarea rows="6" id="url" class="form-control" data-notnull="false" data-alert=""></textarea>
            </div>
        </div>
    </div>

</main>
<footer></footer>

<script>
    $(function(){
        // 编辑
        // 提交
        $('#submit').on("click", function(){
            var $btn = $(this);
            $btn.attr('disabled', true);
            var params = validAndPackingParams($('#paramsForm').find('input[type=text],textarea'));
            if (params == null) return;
            // Ajax提交
            $.post('/mock-web/ops/authcoll', params, function(data){
                var msg = data.msg;
                if (data.code == 0) {
//                    alert("编辑成功");
                    $('#url').html(data.msg).focus();
                    $btn.attr('disabled', false);
                    return;
                }
                msg || (msg = "系统错误");
                alert(msg);
                $('#server').select();
                $('#submit').attr('disabled', false);
            }).error(function (data) {
                alert('网络错误，请重试');
                $('#submit').attr('disabled', false);
            });
        });

    });

    function validAndPackingParams($inps) {
        var params = {};
        for(var i = 0; i < $inps.size(); i++) {
            var $e = $inps.eq(i), notnull = $e.data('notnull'), val = $e.val();
            if (notnull && CMM.isBlank(val)) {
                alert($e.data("alert"));
                $e.focus();
                return null;
            }
            params[$e.attr('name')] = val;
        }
        return params;
    }

    function writeDateToDom(info) {
        $.each(info, function (k, v) {
            $('#'+k).val(v);
        })
    }
</script>
</body>
</html>