<%--
  Created by IntelliJ IDEA.
  User: Fyduan
  Date: 2016/5/1
  Time: 8:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/common/taglibs.jspf" %>
<html>
<head>
    <mw:pageHtmlHead title="预览配置" />
</head>
<body>
    <mw:pageBodyHeader/>
    <main class="container">
        <div class="row">
            <div class="form-inline">
                <div class="form-group hide">
                    <label for="mockId">MOCK编号：</label>
                    <input type="text" class="form-control" id="mockId" value="" placeholder="">
                </div>
                <div class="form-group">
                    <label for="paramType">参数类型（paramType）:</label>
                    <input type="text" class="form-control" id="paramType" value="STRING" placeholder="">
                </div>

                <div class="form-group">
                    <label for="params">参数（params）:</label>
                    <input type="text" class="form-control" id="params" placeholder="">
                </div>
                <button type="submit" id="btnMock" class="btn btn-default">提交</button>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="row"></div>
            <div class="row">
                <pre id="showMockResult"></pre>
            </div>
        </div>
    </main>

<script>
    $(function () {
        var $mockId = $('#mockId'), $paramType = $('#paramType');
        window._preview_id_ = CMM.getSearchParam('id', location.search);
        if (CMM.isBlank(_preview_id_)) {
            $mockId.parent().removeClass('hide');
        } else {
            $mockId.val(_preview_id_);
        }

        // 提交参数
        $('#btnMock').on('click', function (e) {
            var mockId = $mockId.val(), paramType = $paramType.val(), params = $('#params').val();
            if (CMM.isBlank(mockId)) {
                alert('MOCK配置的编号不能为空');
                $mockId.focus();
                return;
            }
            if (CMM.isBlank(paramType)) {
                alert('参数类型不能为空，支持：LONG/INTEGER/STRING/BOOLEAN/MAP 等');
                $paramType.focus();
                return false;
            }
            $.ajax({
                url: 'mock',
                type: 'POST',
                dataType: 'text',
                data: {'id':mockId, 'paramType':paramType, 'params':params},
                success: function (data) {
                    $('#showMockResult').text(data);
                }
            });
        });
        //
    })
</script>
</body>
</html>
