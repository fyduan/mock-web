$(function(){
    window.CMM={};
    CMM.HOST = "";
    CMM.CONTEXT_PATH = "";
    "use strict";
    /***
     * 解决ie8bind不支持
     */
    if (!Function.prototype.bind) {
        Function.prototype.bind = function (oThis) {
            if (typeof this !== "function") {
                // closest thing possible to the ECMAScript 5 internal IsCallable function
                throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
            }

            var aArgs = Array.prototype.slice.call(arguments, 1),
                fToBind = this,
                fNOP = function () {},
                fBound = function () {
                    return fToBind.apply(this instanceof fNOP && oThis ? this : oThis || window,
                        aArgs.concat(Array.prototype.slice.call(arguments)));
                };

            fNOP.prototype = this.prototype;
            fBound.prototype = new fNOP();

            return fBound;
        };}
    /**
     * cookie操作接口
     * @param name {String}
     * @param val {String}
     */
    CMM.session = function(name, val){
        if(arguments.length > 1){
            $.removeCookie(name, {path : '/'});
            if(val){
                $.cookie(name, val, {path : '/'});
            }
        }else{
            return $.cookie(name);
        }
    };
    CMM.removeCookie = function(name){
        $.removeCookie(name, {path : '/'});
    };
    /**
     * 判断传入的值是否为空
     * @param html {} 传入值
     * @param 返回true/false
     */
    CMM.isBlank = function (s) {
        return (!s || $.trim(s) === "");
    };
    /**
     * 手机号码验证
     */
    CMM.isMobilePhone = function (value) {
        var b = false,
            reg = /^0{0,1}(13[0-9]|15[0-9]|14[0-9]|18[0-9]|17[0-9]|16[0-9])[0-9]{8}$/;
        if (!CMM.isBlank(value)) {
            b = reg.test(value);
        }
        return b;
    };
    // 得到浏览器的信息
    CMM.Sys = (function(ua) {
        var s = {};
        s.IE = ua.match(/(msie\s|trident.*rv:)([\w.]+)/) ? true: false;
        s.IE11 = ua.match(/(mozilla\s|trident.*rv:11)([\w.]+)/) ? true: false;
        s.Firefox = ua.match(/firefox\/([\d.]+)/) ? true: false;
        s.Chrome = ua.match(/chrome\/([\d.]+)/) ? true: false;
        s.IE6 = s.IE && ([/msie 6\.0/g.test(ua)][0]);
        s.IE7 = s.IE && ([/msie 7\.0/g.test(ua)][0]);
        s.IE8 = s.IE && ([/msie 8\.0/g.test(ua)][0]);
        return s;
    })(navigator.userAgent.toLowerCase());
    /**
     * 文件检测，包括类型和大小
     * @param $fileDom {jquery dom} file空间jquery对象
     * @param size {number} 文件的大小，如果有值则使用传入的size作为未见的maxsize
     */
    CMM.checkFileSize = function ($fileDom, size) {
        var file_maxsize = size || 2*1024*1024/* 2M */,
            errMsg = "上传的附件文件不能超过__SIZE__M！！！",
            tipMsg = "您的浏览器暂不支持计算上传文件的大小，确保上传文件不要超过__SIZE__M，建议使用IE、FireFox、Chrome浏览器。",
            filesize = 0/* 存放file的size */;
        try {
            if(this.isBlank($fileDom.val())){
                return false;
            }

            /**
             * 不同浏览器表现不一
             * 在FireFox、Chrome浏览器中可以根据
             * document.getElementById(“fileid”).files[0].size
             * 获取上传文件的大小（字节数）;
             * 而IE浏览器中不支持该属性，只能借助<img>标签的dynsrc属性，来
             * 间接实现获取文件的大小（但需要同意ActiveX控件的运行）。
             */
            if (CMM.Sys.Firefox || CMM.Sys.Chrome) {
                filesize = $fileDom[0].files[0].size;
            } else if (CMM.Sys.IE) {
                // 如果是IE，则在body中添加一个临时IMG元素，display=none；
                var $tempImg = $('<img src="'+$fileDom.val()+'" id="temp-img" data-id="'+$fileDom[0].id+'" />').appendTo('body');
                $tempImg[0].dynsrc=$fileDom.val();
                filesize = $tempImg[0].fileSize;
            } else {
                alert(tipMsg.replace('__SIZE__', (file_maxsize/(1024*1024))+''));
                return false;
            }

            // 判断filesize大小
            if (filesize == -1) {
                alert(tipMsg.replace('__SIZE__', (file_maxsize/(1024*1024))+''));
                return false;
            } else if(filesize > file_maxsize) {
                alert(errMsg.replace('__SIZE__', (file_maxsize/(1024*1024))+''));
                return false;
            } else {
                return true;
            }
        } catch (e) {
            alert(e);
            return false;
        }
    };
    /**
     *
     */
        // 解析url请求参数
     CMM.getSearchParam = function(k, url){
        var v = "", arrp, key, arrp_len, arrp_i, value, i;

        if (CMM.isBlank(url)) {
            url=location.search;
            if(CMM.isBlank(url)) {
                return v;
            }
        }
        arrp = url.substring(1).split("&");

        for(i=0, arrp_len = arrp.length; i<arrp_len; i++){

            arrp_i = arrp[i];

            if (CMM.isBlank(arrp_i)) {
                break;
            }

            key = arrp[i].split("=")[0];
            value = arrp[i].split("=")[1];

            if(key == k){
                v = value;
                break;
            }
        }
        return this.Sys.IE ? v : decodeURIComponent(v);
    };
    /**
     * 时间戳转化为"yyyy/mm/dd"格式
     * @param time
     * @returns {string}
     */
    CMM.formateTime=function(time){
        var date=new Date(time);
        var o = {
            "Y" : date.getFullYear(),//year
            "M" : date.getMonth()+1, //month
            "d" : date.getDate()//day
        };
        return o.Y+"/"+ o.M+"/"+ o.d;
    };
    /**
     * 显示通知发送的时间
     * @param time
     * @returns {string}
     * 若为当天时间，不显示日。只显示时：分。
     昨天，前天，显示为昨天/前天＋时：分。例如：昨天  10:29
     今年内显示为：月日。例如：1月1日
     更早时间，显示：年月。例如：2013年1月1日
     */
    CMM.formatTime = function (time) {
        var date=new Date(time),
            now=new Date(),
            year=date.getFullYear(),
            moth=date.getMonth()+1,
            day=date.getDate(),
            str_moth=moth<10?("0"+moth):moth,
            str_day=day<10?("0"+day):day,
            str_time="",
            h=date.getHours(),
            m=date.getMinutes(),
            str_h=h<10?("0"+h):h,
            str_m=m<10?("0"+m):m;
        if ( now.getFullYear() > year ) str_time += year+"年"+str_moth+"月"+str_day+"日";
        else {
            if(Math.floor(now.getTime()/3600/24/1000)-Math.floor(date.getTime()/3600/24/1000) == 0 ) str_time += "";
            else if(Math.floor(now.getTime()/3600/24/1000)-Math.floor(date.getTime()/3600/24/1000) == 1) str_time += "昨天";
            else if(Math.floor(now.getTime()/3600/24/1000)-Math.floor(date.getTime()/3600/24/1000) == 2) str_time += "前天";
            else if(Math.floor(now.getTime()/3600/24/1000)-Math.floor(date.getTime()/3600/24/1000) <= 7) {
                str_time += "星期";
                switch (date.getDay()){
                    case 0:{
                        str_time += "日";
                        break;
                    }
                    case 1:{
                        str_time += "一";
                        break;
                    }
                    case 2:{
                        str_time += "二";
                        break;
                    }
                    case 3:{
                        str_time += "三";
                        break;
                    }
                    case 4:{
                        str_time += "四";
                        break;
                    }
                    case 5:{
                        str_time += "五";
                        break;
                    }
                    case 6:{
                        str_time += "六";
                        break;
                    }
                }
            }
            else str_time += str_moth+"月"+str_day+"日"
        }
        str_time += " "+ str_h + ":" + str_m;
        return str_time;
    };
    /**
     * 文件大小格式转换
     * @param size
     * @returns {*}
     */
    CMM.formatFileSize = function(size) {

        if (size === undefined || /\D/.test(size)) {
            return 'N/A';
        }

        function round(num, precision) {
            return Math.round(num * Math.pow(10, precision)) / Math.pow(10, precision);
        }

        var boundary = Math.pow(1024, 4);

        // TB
        if (size > boundary) {
            return round(size / boundary, 2) + " " + 'T';
        }

        // GB
        if (size > (boundary/=1024)) {
            return round(size / boundary, 2) + " " + 'G';
        }

        // MB
        if (size > (boundary/=1024)) {
            return round(size / boundary, 2) + " " + 'M';
        }

        // KB
        if (size > 1024) {
            return round(size / 1024, 2) + " " + 'K';
        }

        return size + " " + 'B';
    };

    /**
     * 统计字符串的长度，中文两个字符，英文一个字符
     * @param text
     */
    CMM.getTextLength = function(text){
        var sum = 0;
        if(!text) return 0;
        for(var i=0;i<text.length;i++){
            if(/[\u4e00-\u9fa5]/.test(text.substring(i,i+1))){
                sum+=2;
            }else{
                sum++;
            }
        }
        return sum;
    };
    /**
     * 取一定长度的字符串，中文两个字符，英文一个字符
     * @param len
     * @param text
     * @returns {*}
     */
    CMM.subTextByLength = function(len, text){
        if(!len || !text || len<0) return "";
        var str = "",textLen=0;
        for(var i=0;i<len;i++){
            if(/[\u4e00-\u9fa5]/.test(text.substring(i,i+1))){
                if(textLen+2<=len){
                    textLen +=2;
                    str+=text.substring(i,i+1);
                }else{
                    break;
                }
            }else{
                if(textLen+1<=len) {
                    textLen += 1;
                    str += text.substring(i, i + 1);
                } else {
                    break;
                }
            }
        }
        return str;
    }
    CMM.checkClsName=function(text){
        if (0 < CMM.getTextLength(text) && CMM.getTextLength(text)<= 40) {
            return true;
        } else {
            return false;
        }
    };
    CMM.checkUsrName=function(text){
        if (0 < CMM.getTextLength(text) && CMM.getTextLength(text) <= 16) {
            return true;
        } else {
            return false;
        }
    };
    CMM.checkNumber=function(text){
        if (!(/\D/g).test(text)) {
            return true;
        } else {
            return false;
        }
    };
    /**
     * 去除数组重复元素
     */
    CMM.uniqueArray =function(data){
        data = data || [];
        var a = {};
        for (var i=0; i<data.length; i++) {
            var v = data[i];
            if (typeof(a[v]) == 'undefined'){
                a[v] = 1;
            }
        };
        data.length=0;
        for (var i in a){
            data[data.length] = i;
        }
        return data;
    }

    /**
     *去除字串串两端的空格
     */
    String.prototype.trim = function () {
        return this .replace(/^\s\s*/, '' ).replace(/\s\s*$/, '' );
    }

    /**
     * MD5工具
     * @param string 加密字符串
     * @returns {string}
     */
    CMM.md5 = function (string) {

        function RotateLeft(lValue, iShiftBits) {
            return (lValue<<iShiftBits) | (lValue>>>(32-iShiftBits));
        }

        function AddUnsigned(lX,lY) {
            var lX4,lY4,lX8,lY8,lResult;
            lX8 = (lX & 0x80000000);
            lY8 = (lY & 0x80000000);
            lX4 = (lX & 0x40000000);
            lY4 = (lY & 0x40000000);
            lResult = (lX & 0x3FFFFFFF)+(lY & 0x3FFFFFFF);
            if (lX4 & lY4) {
                return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
            }
            if (lX4 | lY4) {
                if (lResult & 0x40000000) {
                    return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
                } else {
                    return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
                }
            } else {
                return (lResult ^ lX8 ^ lY8);
            }
        }

        function F(x,y,z) { return (x & y) | ((~x) & z); }
        function G(x,y,z) { return (x & z) | (y & (~z)); }
        function H(x,y,z) { return (x ^ y ^ z); }
        function I(x,y,z) { return (y ^ (x | (~z))); }

        function FF(a,b,c,d,x,s,ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        };

        function GG(a,b,c,d,x,s,ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        };

        function HH(a,b,c,d,x,s,ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        };

        function II(a,b,c,d,x,s,ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        };

        function ConvertToWordArray(string) {
            var lWordCount;
            var lMessageLength = string.length;
            var lNumberOfWords_temp1=lMessageLength + 8;
            var lNumberOfWords_temp2=(lNumberOfWords_temp1-(lNumberOfWords_temp1 % 64))/64;
            var lNumberOfWords = (lNumberOfWords_temp2+1)*16;
            var lWordArray=Array(lNumberOfWords-1);
            var lBytePosition = 0;
            var lByteCount = 0;
            while ( lByteCount < lMessageLength ) {
                lWordCount = (lByteCount-(lByteCount % 4))/4;
                lBytePosition = (lByteCount % 4)*8;
                lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount)<<lBytePosition));
                lByteCount++;
            }
            lWordCount = (lByteCount-(lByteCount % 4))/4;
            lBytePosition = (lByteCount % 4)*8;
            lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80<<lBytePosition);
            lWordArray[lNumberOfWords-2] = lMessageLength<<3;
            lWordArray[lNumberOfWords-1] = lMessageLength>>>29;
            return lWordArray;
        };

        function WordToHex(lValue) {
            var WordToHexValue="",WordToHexValue_temp="",lByte,lCount;
            for (lCount = 0;lCount<=3;lCount++) {
                lByte = (lValue>>>(lCount*8)) & 255;
                WordToHexValue_temp = "0" + lByte.toString(16);
                WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length-2,2);
            }
            return WordToHexValue;
        };

        function Utf8Encode(string) {
            string = string.replace(/\r\n/g,"\n");
            var utftext = "";

            for (var n = 0; n < string.length; n++) {

                var c = string.charCodeAt(n);

                if (c < 128) {
                    utftext += String.fromCharCode(c);
                }
                else if((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
                else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }

            }

            return utftext;
        };

        var x=Array();
        var k,AA,BB,CC,DD,a,b,c,d;
        var S11=7, S12=12, S13=17, S14=22;
        var S21=5, S22=9 , S23=14, S24=20;
        var S31=4, S32=11, S33=16, S34=23;
        var S41=6, S42=10, S43=15, S44=21;

        string = Utf8Encode(string);

        x = ConvertToWordArray(string);

        a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;

        for (k=0;k<x.length;k+=16) {
            AA=a; BB=b; CC=c; DD=d;
            a=FF(a,b,c,d,x[k+0], S11,0xD76AA478);
            d=FF(d,a,b,c,x[k+1], S12,0xE8C7B756);
            c=FF(c,d,a,b,x[k+2], S13,0x242070DB);
            b=FF(b,c,d,a,x[k+3], S14,0xC1BDCEEE);
            a=FF(a,b,c,d,x[k+4], S11,0xF57C0FAF);
            d=FF(d,a,b,c,x[k+5], S12,0x4787C62A);
            c=FF(c,d,a,b,x[k+6], S13,0xA8304613);
            b=FF(b,c,d,a,x[k+7], S14,0xFD469501);
            a=FF(a,b,c,d,x[k+8], S11,0x698098D8);
            d=FF(d,a,b,c,x[k+9], S12,0x8B44F7AF);
            c=FF(c,d,a,b,x[k+10],S13,0xFFFF5BB1);
            b=FF(b,c,d,a,x[k+11],S14,0x895CD7BE);
            a=FF(a,b,c,d,x[k+12],S11,0x6B901122);
            d=FF(d,a,b,c,x[k+13],S12,0xFD987193);
            c=FF(c,d,a,b,x[k+14],S13,0xA679438E);
            b=FF(b,c,d,a,x[k+15],S14,0x49B40821);
            a=GG(a,b,c,d,x[k+1], S21,0xF61E2562);
            d=GG(d,a,b,c,x[k+6], S22,0xC040B340);
            c=GG(c,d,a,b,x[k+11],S23,0x265E5A51);
            b=GG(b,c,d,a,x[k+0], S24,0xE9B6C7AA);
            a=GG(a,b,c,d,x[k+5], S21,0xD62F105D);
            d=GG(d,a,b,c,x[k+10],S22,0x2441453);
            c=GG(c,d,a,b,x[k+15],S23,0xD8A1E681);
            b=GG(b,c,d,a,x[k+4], S24,0xE7D3FBC8);
            a=GG(a,b,c,d,x[k+9], S21,0x21E1CDE6);
            d=GG(d,a,b,c,x[k+14],S22,0xC33707D6);
            c=GG(c,d,a,b,x[k+3], S23,0xF4D50D87);
            b=GG(b,c,d,a,x[k+8], S24,0x455A14ED);
            a=GG(a,b,c,d,x[k+13],S21,0xA9E3E905);
            d=GG(d,a,b,c,x[k+2], S22,0xFCEFA3F8);
            c=GG(c,d,a,b,x[k+7], S23,0x676F02D9);
            b=GG(b,c,d,a,x[k+12],S24,0x8D2A4C8A);
            a=HH(a,b,c,d,x[k+5], S31,0xFFFA3942);
            d=HH(d,a,b,c,x[k+8], S32,0x8771F681);
            c=HH(c,d,a,b,x[k+11],S33,0x6D9D6122);
            b=HH(b,c,d,a,x[k+14],S34,0xFDE5380C);
            a=HH(a,b,c,d,x[k+1], S31,0xA4BEEA44);
            d=HH(d,a,b,c,x[k+4], S32,0x4BDECFA9);
            c=HH(c,d,a,b,x[k+7], S33,0xF6BB4B60);
            b=HH(b,c,d,a,x[k+10],S34,0xBEBFBC70);
            a=HH(a,b,c,d,x[k+13],S31,0x289B7EC6);
            d=HH(d,a,b,c,x[k+0], S32,0xEAA127FA);
            c=HH(c,d,a,b,x[k+3], S33,0xD4EF3085);
            b=HH(b,c,d,a,x[k+6], S34,0x4881D05);
            a=HH(a,b,c,d,x[k+9], S31,0xD9D4D039);
            d=HH(d,a,b,c,x[k+12],S32,0xE6DB99E5);
            c=HH(c,d,a,b,x[k+15],S33,0x1FA27CF8);
            b=HH(b,c,d,a,x[k+2], S34,0xC4AC5665);
            a=II(a,b,c,d,x[k+0], S41,0xF4292244);
            d=II(d,a,b,c,x[k+7], S42,0x432AFF97);
            c=II(c,d,a,b,x[k+14],S43,0xAB9423A7);
            b=II(b,c,d,a,x[k+5], S44,0xFC93A039);
            a=II(a,b,c,d,x[k+12],S41,0x655B59C3);
            d=II(d,a,b,c,x[k+3], S42,0x8F0CCC92);
            c=II(c,d,a,b,x[k+10],S43,0xFFEFF47D);
            b=II(b,c,d,a,x[k+1], S44,0x85845DD1);
            a=II(a,b,c,d,x[k+8], S41,0x6FA87E4F);
            d=II(d,a,b,c,x[k+15],S42,0xFE2CE6E0);
            c=II(c,d,a,b,x[k+6], S43,0xA3014314);
            b=II(b,c,d,a,x[k+13],S44,0x4E0811A1);
            a=II(a,b,c,d,x[k+4], S41,0xF7537E82);
            d=II(d,a,b,c,x[k+11],S42,0xBD3AF235);
            c=II(c,d,a,b,x[k+2], S43,0x2AD7D2BB);
            b=II(b,c,d,a,x[k+9], S44,0xEB86D391);
            a=AddUnsigned(a,AA);
            b=AddUnsigned(b,BB);
            c=AddUnsigned(c,CC);
            d=AddUnsigned(d,DD);
        }

        var temp = WordToHex(a)+WordToHex(b)+WordToHex(c)+WordToHex(d);

        return temp.toLowerCase();
    };
});