package com.shulidata.mock

import com.alibaba.fastjson.JSONObject

class GroovyTest {

    public Object doJob(Object o) {
        JSONObject s = new JSONObject();
        s.put("h1", "ha1");
        s.put("h2", "ha2");
        s.put("h3", "ha3");
        s.put("h4", "ha4");

        return s;
    }

}
