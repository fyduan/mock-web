package com.shulidata.mock;

import com.shulidata.mock.model.MockConf;
import com.shulidata.mock.service.MockConfService;
import com.shulidata.mock.utils.GenericException;
import com.shulidata.mock.utils.UniqueIdUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

/**
 * @author fyduan
 * @date 16/4/29
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration("classpath:/spring/spring-deploy.xml")
public class MockConfServiceTest {

    @Autowired
    MockConfService confService;

    @Test
    public void testSave() throws GenericException {
        MockConf conf = new MockConf();
        conf.setApi("testApi");
        conf.setServer("testServer");
        conf.setDesc("test desc");
        conf.setScript("System.out.println(1);");
        conf.setModifyTime(System.currentTimeMillis());
        confService.saveOrUpdate(conf);

        Assert.assertNotNull(conf.getId());
    }

    @Test
    public void testGetConf() {
        MockConf conf = confService.findById(UniqueIdUtils.compressUuid());
        Assert.assertNotNull(conf);
    }

    @Test
    public void testLoadAll() {
        List<MockConf> list = confService.loadAll();
        Assert.assertNotNull(list);
    }

    @Test
    public void testRandom() {
        System.out.println((long)(Math.random() * 10000));
        try {
            Thread.sleep((long)(Math.random() * 10000));
        } catch (InterruptedException e) {
        }
    }

}
